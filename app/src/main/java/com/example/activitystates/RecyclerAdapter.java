package com.example.activitystates;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;

import java.util.List;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.RecyclerViewholder> {
    private List<Example> movieList;
    private Context context;

    public RecyclerAdapter(Context context, List<Example> movieList) {
        this.movieList = movieList;
        this.context = context;
    }

    public void setMovieList(List<Example> movieList) {
        this.movieList = movieList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerViewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter, parent, false);
        return new RecyclerAdapter.RecyclerViewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewholder holder, int position) {
        holder.tvMovieName.setText(movieList.get(position).getTitle().toString());
        String url= movieList.get(position).getImage();
        Glide.with(context).load(url).error(R.drawable.ic_launcher_background).addListener(new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                Log.i("TAG------",e.getMessage());
                return true;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                return false;
            }
        }).into(holder.image);
    }

    @Override
    public int getItemCount() {
        if (movieList != null) {
            return movieList.size();
        } else return 0;

    }

    public class RecyclerViewholder extends RecyclerView.ViewHolder {
        TextView tvMovieName;
        ImageView image;

        public RecyclerViewholder(@NonNull View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.image);
            tvMovieName = itemView.findViewById(R.id.title);
        }
    }
}