package com.example.activitystates;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.MovieViewHolder> {
    private  List<Example> movieList;
    private final Context context;

    public RecyclerViewAdapter(Context context,List<Example> movieList) {
        this.movieList = movieList;
        this.context = context;
    }
    public void setMovieList(List<Example> movieList) {
        this.movieList = movieList;
        notifyDataSetChanged();
    }
    @NonNull
    @Override
    public RecyclerViewAdapter.MovieViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_items,parent,false);
        return new MovieViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MovieViewHolder holder, int position) {
        holder.tvMovieName.setText(movieList.get(position).getTitle().trim());

        Glide.with(context).load(movieList.get(position).getImage()).apply(RequestOptions.centerCropTransform()).into(holder.image);
    }
/*

    @Override
    public void RecyclerViewAdapter.MovieViewHolder onBin(@NonNull MovieViewHolder holder, int position) {
        holder.tvMovieName.setText(movieList.get(position).getTitle().trim());

        Glide.with(context).load(movieList.get(position).getImage()).apply(RequestOptions.centerCropTransform()).into(holder.image);
    }
*/

    @Override
    public int getItemCount() {
        if(movieList != null){
            return movieList.size();
        }
        return 0;
    }

    public class MovieViewHolder extends RecyclerView.ViewHolder {
        TextView tvMovieName;
        ImageView image;

        public MovieViewHolder(@NonNull View itemView) {

            super(itemView);
            image=itemView.findViewById(R.id.image);
            tvMovieName = itemView.findViewById(R.id.text);

        }
    }
}
