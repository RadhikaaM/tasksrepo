package com.example.activitystates;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button button=findViewById(R.id.goto1);

        Log.i("tag Activity1", "OnCreate");
        
        

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this, MainActivity2.class);
                startActivity(intent);
            }
        });


    }


    @Override
    protected void onStart() {
        Log.i("tag Activity1","onStart");
        super.onStart();
    }

    @Override
    protected void onResume() {
        Log.i("tag Activity1","onResume");
        super.onResume();
    }

    @Override
    protected void onPause() {
        Log.i("tag Activity1","onPause");
        super.onPause();
    }

    @Override
    protected void onStop() {
        Log.i("tag Activity1","onStop");
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        Log.i("tag Activity1","onDestroy");
        super.onDestroy();
    }
}