package com.example.activitystates;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        Log.i("tag Activity2", "OnCreate");
        Button button=findViewById(R.id.goto2);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity2.this, MainActivity.class);
                startActivity(intent);
            }
        });


    }
    @Override
    protected void onStart() {
        Log.i("tag Activity2","onStart");
        super.onStart();
    }

    @Override
    protected void onResume() {
        Log.i("tag Activity2","onResume");
        super.onResume();
    }

    @Override
    protected void onPause() {
        Log.i("tag Activity2","onPause");
        super.onPause();
    }

    @Override
    protected void onStop() {
        Log.i("tag Activity2","onStop");
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        Log.i("tag Activity2","onDestroy");
        super.onDestroy();
    }
}