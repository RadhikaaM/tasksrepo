package com.example.activitystates;


import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface MovieServiceInterface {
    @GET("volley_array.json")
    Call<List<Example>> getMovies();

}
